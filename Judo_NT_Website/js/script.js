// Navigation Button Functions
function collapseMenu() {
  document.getElementById("nav-links").style.display = "none";
}

function displayMenu() {
  document.getElementById("nav-links").style.display = "block";
}

/* READ MORE BUTTON */
readMoreBtn = document.addEventListener('onclick', expandText);

function expandText() {
  const readMoreBtn = document.querySelector('.readMoreBtn');
  const text = document.querySelector('.text');

  text.classList.toggle("show-more");
  if (readMoreBtn.innerText === 'Read More') {
    readMoreBtn.innerText = 'Read Less';
  }
  else {
    readMoreBtn.innerText = 'Read More';
  }
}