/* Used Andrew's Code for Validation from Sprint 2 Registration Page */
/* Edited to add more functionality / relevant changes */
/* Password Verification does work but still says successful registration even with password error, not sure how to fix yet */

function passwordToggle() {
    var toggle = document.getElementById("password");
    if (toggle.type === "password") {
        toggle.type = "text";
    } else {
        toggle.type = "password";
    }
}

function password_repeatToggle() {
    var toggle = document.getElementById("password-repeat");
    if (toggle.type === "password") {
        toggle.type = "text";
    } else {
        toggle.type = "password";
    }
}    


function checkAge() {
    var date_input = document.forms["registerForm"]["birth_date"];
    var birth_date = new Date(date_input.value);
    var difference = Date.now() - birth_date.getTime();
    var ageDate = new Date(difference);
    var age = Math.abs(ageDate.getUTCFullYear() - 1970)
    return age;
}

function checkForm() {


    var password = document.getElementById("password");
    var passwordRepeat = document.getElementById("password-repeat");
    var password_error = document.getElementById("password_error");
    if (passwordRepeat != password) {
        password_error.innerHTML = "Passwords do not match!";
        passwordRepeat.style.border = "1px solid red";
        passwordRepeat.focus();
        return false;
    }

    var countrycode_error = document.getElementById("countrycode_error")
    var country_code = document.getElementById("country_code")
    if (country_code.value < 1) {
        countrycode_error.innerHTML = "Please enter correct country code";
        country_code.style.border = "1px solid red";
        country_code.focus();
        return false;
    }

    var birthdate_error = document.getElementById("birthdate_error")
    var date_of_birth = document.getElementById("birth_date")
    if (checkAge() < 13) {
        birthdate_error.innerHTML = "You must be over the age of 13 years old to register";
        date_of_birth.style.border = "1px solid red";
        date_of_birth.focus();
        return false;
    }

    if (checkAge() > 50) {
        birthdate_error.innerHTML = "Refer to https://nationalseniors.com.au/technology for assistance.";
        date_of_birth.style.border = "1px solid red";
        date_of_birth.focus();
        return false;
    }

    

    else {
        alert("Congratulations, you have successfully registered an account with Judo NT.");
        return true;
    }
}