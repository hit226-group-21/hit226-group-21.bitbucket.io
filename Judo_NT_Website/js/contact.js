function validateName() {
    let name = document.getElementById('contact_name').value;

    if (name == "") {
        document.getElementById("validate_name_error").style.display = "block";
        document.getElementById('contact_name').style.borderColor = "red";
        document.getElementById("validate_name_error").innerHTML = "This field is required."
        return false;
    } else {
        document.getElementById("validate_name_error").style.display = "none";
        document.getElementById('contact_name').style.borderColor = "#dfdfeb";
        return true;
    }
}

function validateEmail() {
    let email = document.getElementById('contact_email').value;

    if (email == "") {
        document.getElementById("validate_email_error").style.display = "block";
        document.getElementById('contact_email').style.borderColor = "red";
        document.getElementById("validate_email_error").innerHTML = "This field is required."
        return false;
    } else {
        document.getElementById("validate_email_error").style.display = "none";
        document.getElementById('contact_email').style.borderColor = "#dfdfeb";
        return true;
    }
}

function validatePhoneNumber() {
    let phone_number = document.getElementById("contact_phone").value;

    if (phone_number == "") {
        document.getElementById("contact_phone").value = "Not Specified";
        return true;
    }
}

function validateMessage() {
    let message = document.getElementById('contact_message').value;

    if (message == "") {
        document.getElementById("validate_message_error").style.display = "block";
        document.getElementById('contact_message').style.borderColor = "red";
        document.getElementById("validate_message_error").innerHTML = "This field is required."
        return false;
    } else {
        document.getElementById("validate_message_error").style.display = "none";
        document.getElementById('contact_message').style.borderColor = "#dfdfeb";
        return true;
    }
}

function validateBotCheck() {
    let botcheck = document.getElementById('contact_botcheck').checked;

    if (botcheck == false) {
        document.getElementById("validate_botcheck_error").style.display = "block";
        document.getElementById("validate_botcheck_error").innerHTML = "This checkbox must be ticked."
        return false;
    } else {
        document.getElementById("validate_botcheck_error").style.display = "none";
        return true;
    }
}


function validateContactForm() {
    if (validateName() && validateEmail() && validatePhoneNumber() && validateMessage() && validateBotCheck()) {
        sendEmail();
    }
}

// G-Mail Account Information
// Email: hit226.group21@gmail.com
// Password: HIT226Group21!

function sendEmail() {
    Email.send({
        Host: "smtp.elasticemail.com",
        Username: "hit226.group21@gmail.com",
        Password: "ABE81F55BD0B2C0A44785CCE8EAF2C2270AD",
        To: 'hit226.group21@gmail.com',
        From: document.getElementById("contact_email").value,
        Subject: "Contact Form Enquiry",
        Body: "<h3>Name: </h3>" + document.getElementById("contact_name").value
            + "<br> <h3>Email: </h3>" + document.getElementById("contact_email").value
            + "<br> <h3>Phone Number: </h3>" + document.getElementById("contact_phone").value
            + "<br> <h3>Message: </h3>" + document.getElementById("contact_message").value
    }).then(
        message => alert(message)
    );
}