const images = document.querySelectorAll(".gallery-item img");
let imgIndex;
let imgSrc;
// get images src onclick
images.forEach((img, i) => {
    img.addEventListener("click", (e) => {
        imgSrc = e.target.src;
        //run modal function
        imgModal(imgSrc);
        //index of the next image
        imgIndex = i;
    });
});
//creating the modal
let imgModal = (src) => {
    const modal = document.createElement("div");
    modal.setAttribute("class", "modal");
    //add modal to the parent element 
    document.querySelector(".main-gallery").append(modal);
    //adding image to modal
    const newImage = document.createElement("img");
    newImage.setAttribute("src", src);
    //creating the close button
    const closeBtn = document.createElement("i");
    closeBtn.setAttribute("class", "fa-solid fa-x close-btn");
    //close function
    closeBtn.onclick = () => {
        modal.remove();
    };
    document.addEventListener('keydown', function (event) {
        if (event.key === "Escape") {
            modal.remove();
        }
    })
    //next and previous buttons
    // change the src of current image to the src of pevious image
    const prevBtn = document.createElement("i");
    prevBtn.setAttribute("class", "fa-solid fa-angle-left prev-btn");
    prevBtn.onclick = () => {
        newImage.setAttribute("src", prevImg())
    }
    const nextBtn = document.createElement("i");
    nextBtn.setAttribute("class", "fa-solid fa-angle-right next-btn");
    nextBtn.onclick = () => {
        newImage.setAttribute("src", nextImg())
    };
    // document.addEventListener('keyup', function (event) {
    //     if (event.key === "ArrowLeft") {
    //         newImage.setAttribute("src", prevImg())
    //     }
    //     else if (event.key === "ArrowRight") {
    //         newImage.setAttribute("src", nextImg())
    //     }
    // }, false);

    modal.append(prevBtn, newImage, nextBtn, closeBtn);
};
//next image function
let nextImg = () => {
    imgIndex++;
    console.log(imgIndex);
    //check if it is the the last image
    if (imgIndex >= images.length) {
        imgIndex = 0
    }
    //return src of the new image
    return images[imgIndex].src;
};
//previous image function
let prevImg = () => {
    imgIndex--;
    console.log(imgIndex);
    //check if it is the first image
    if (imgIndex < 0) {
        imgIndex = images.length - 1
    }
    //return src of previous image
    return images[imgIndex].src
}