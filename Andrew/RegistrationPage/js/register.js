function passwordToggle() {
    var toggle = document.getElementById("password");
    if (toggle.type === "password") {
        toggle.type = "text";
    } else {
        toggle.type = "password";
    }
}


function checkAge() {
    var date_input = document.forms["registerForm"]["birth_date"];
    var birth_date = new Date(date_input.value);
    var difference = Date.now() - birth_date.getTime();
    var ageDate = new Date(difference);
    var age = Math.abs(ageDate.getUTCFullYear() - 1970)
    return age;
}

function checkForm() {

    var countrycode_error = document.getElementById("countrycode_error")
    var country_code = document.getElementById("country_code")
    if (country_code.value < 1) {
        countrycode_error.innerHTML = "Please enter correct country code";
        country_code.style.border = "1px solid red";
        country_code.focus();
        return false;
    }

    var birthdate_error = document.getElementById("birthdate_error")
    var date_of_birth = document.getElementById("birth_date")
    if (checkAge() < 13) {
        birthdate_error.innerHTML = "You must be over the age of 13 years old to register";
        date_of_birth.style.border = "1px solid red";
        date_of_birth.focus();
        return false;
    }

    if (checkAge() > 50) {
        birthdate_error.innerHTML = "Refer to https://nationalseniors.com.au/technology for assistance.";
        date_of_birth.style.border = "1px solid red";
        date_of_birth.focus();
        return false;
    }

    else {
        alert("Successfully registered");
        return true;
    }
}